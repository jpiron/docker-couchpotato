# Docker CouchPotato
Dockerfile to set up [CouchPotato](https://couchpota.to/)

[![CouchPotato](https://couchpota.to/media/images/couch.png)](https://couchpota.to/)

## Repository
Sources are [here](https://gitlab.com/jpiron/docker-couchpotato).

## Run
```bash
docker pull jpiron/couchpotato
docker run jpiron/couchpotato
```
However, you're more likely to run the container with following:
```bash
docker run -d --restart=always --name couchpotato \
-p 5050:5050 \
--user $(id -u ${USER}):$(id -g ${USER}) \
-e TZ=$(cat /etc/timezone) \
-v "${HOME}/.couchpotato":/couchpotato/datadir \
-v "${HOME}/movies":/movies \
jpiron/couchpotato
```

#### Parameters
* **user**: Docker run parameter to set the user/group to run the container with;
* **TZ**: the timzeone to run the container with;
* **/couchpotato/datadir**: if you want (and you should) CouchPotato data directory to be hold in a local directory. If the directory contains a settings.conf file it will be used to configure CouchPotato.
* **/movies**: provide CouchPotato with an access to your movies directory.

## Caveats
The Dockerfile creates a couchpotato user and makes it the owner of the couchpotato installation folder.
When using the **user** parameter, the container is ran with a different user which is not the owner of the couchpotato installation folder.
As a result, in-software git auto-updates won't work with the **user** parameter.
To perform updates you will have to pull (or build) the latest image version and run it.

## License
MIT

## Versions
* **15.02.2017**: Remove the start.sh entrypoint.
* **05.11.2016**: Remove the PUID/PGID environment variables in favor of the Docker run --user option.
* **13.10.2016**: First release.
