FROM ubuntu:18.04
LABEL maintainer="jonathanpiron@gmail.com"

# Install CouchPotato requirements
RUN apt-get update && apt-get install -y \
        git-core\
        python2.7\
        python-lxml\
        python-openssl\
    && rm -rf /var/lib/apt/lists/*

ARG VERSION=master

# Get CouchPotato.
RUN git clone --depth 1 --branch ${VERSION} https://github.com/CouchPotato/CouchPotatoServer.git /couchpotato

# Create CouchPotato group and user.
RUN groupadd --system couchpotato &&\
    useradd --system -s /bin/false -d /couchpotato -g couchpotato couchpotato

RUN mkdir /couchpotato/datadir /movies\
    && chown -R couchpotato:couchpotato /couchpotato /movies

VOLUME ["/couchpotato/datadir", "/movies"]

EXPOSE 5050

USER couchpotato

ENTRYPOINT ["/usr/bin/python2.7", "/couchpotato/CouchPotato.py", "--data_dir", "/couchpotato/datadir", "--console_log"]
